#!/usr/bin/env python
#-*- coding: utf-8 -*-

from setuptools import setup

setup(name='webrunner',
      version='0.1',
      packages=['webrunner'],
      install_requires=['mechanize', 'beautifulsoup'],
)
