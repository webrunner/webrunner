#!/usr/bin/env python
#-*- coding: utf-8 -*-

import cgi

print 'Content-type: text/html\n'
form = cgi.FieldStorage()
name = form.getvalue('username')
passwd = form.getvalue('password')

print """\
<html>
<head>
<title>Handling POST request</title>
</head>
<body>
<div id="name">%s</div><br/>
<div id="passwd">%s</div><br/>
</body>
</html>
"""  %(name, passwd)
