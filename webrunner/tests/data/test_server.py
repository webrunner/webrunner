#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import BaseHTTPServer, CGIHTTPServer


PORT = 9000
PIDFILE = 'test_server.pid'
THISDIR = os.path.dirname(os.path.realpath(__file__))
os.chdir(THISDIR)

httpd = BaseHTTPServer.HTTPServer(('',PORT), CGIHTTPServer.CGIHTTPRequestHandler)

id = os.fork()
if id == 0:
    pid = os.getpid()
    pidfile = open(PIDFILE, 'w')
    pidfile.write(str(pid))
    pidfile.close()
    a=httpd.serve_forever() 
