#!/usr/bin/env python
#-*- coding: utf-8 -*-

try:
    import webrunner
except ImportError:
    import sys, os
    webrunner_path = '..' + os.sep
    sys.path.append(webrunner_path)

import os
from webrunner import WebBrowser
import urllib
import BaseHTTPServer, CGIHTTPServer
import unittest


server_pidfile = 'test_server.pid'
tests_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = tests_dir + '/data/'

def setup_test_server():

    os.chdir(data_dir)
    os.system('chmod +x test_server.py')
    os.system('./test_server.py 1>&2')
    os.chdir(tests_dir)

def kill_test_server():
    pid = open(data_dir + server_pidfile).read()
    os.kill(int(pid), 9)
    os.remove(data_dir + server_pidfile)

class RunningTheWebTest(unittest.TestCase):

    def setUp(self):
        self.base_url = 'localhost:9000/'
        self.browser = WebBrowser()
        setup_test_server()



    def tearDown(self):
        kill_test_server()


    def test_open_url(self):
        url = self.base_url + 'test0.html'
        self.browser.urlopen(url)
        self.assertTrue(self.browser.current_page is not None)
        self.assertTrue(self.browser.viewing_html())

    def test_check_form(self):
        url = self.base_url + 'test0.html'
        self.browser.urlopen(url)
        page = self.browser.current_page
        self.assertTrue(page.forms.get('form0'))

    def test_send_form_post_password(self):
        url = self.base_url + 'test0.html'
        self.browser.urlopen(url)
        form = self.browser.current_page.forms['form0']
        form.set_value('admin', 'username')
        form.set_value('senha', 'password')
        self.browser.submit_form('form0')
        page = self.browser.current_page
        self.assertTrue(page.title.string == u'Handling POST request')


    def test_follow_link(self):
        url = self.base_url + 'test0.html'
        self.browser.urlopen(url)
        self.browser.follow_link(text='Follow-me!')
        page = self.browser.current_page
        page_title = u'Following link'
        self.assertTrue(page.title.string == page_title)


class WebDocumentTest(unittest.TestCase):
    def setUp(self):
        self.base_url = 'localhost:9000/'
        self.browser = WebBrowser()
        setup_test_server()

    def tearDown(self):
        kill_test_server()

    def test_find_all(self):
        self.browser.urlopen(self.base_url + 'webdoc0.html')
        webdoc = self.browser.current_page

        assert webdoc.findAll('span', {'class' : 'test-span'})

    def test_find(self):
        self.browser.urlopen(self.base_url + 'webdoc0.html')
        webdoc = self.browser.current_page

        assert webdoc.findAll('span', {'class' : 'test-span'})


if __name__ == '__main__':
    unittest.main()
